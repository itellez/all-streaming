-- EstadoCuenta
INSERT INTO estadocuenta (nombre, fechacreacion, usuario_id_creador, fechamod, usuario_id_mod, vigente) VALUES ('Disponible', NOW(), 1, NOW(), 1, 1);
INSERT INTO estadocuenta (nombre, fechacreacion, usuario_id_creador, fechamod, usuario_id_mod, vigente) VALUES ('En Uso', NOW(), 1, NOW(), 1, 1);
INSERT INTO estadocuenta (nombre, fechacreacion, usuario_id_creador, fechamod, usuario_id_mod, vigente) VALUES ('Bloqueada', NOW(), 1, NOW(), 1, 1);