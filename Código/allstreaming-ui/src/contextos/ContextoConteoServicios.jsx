import { useContext } from "react";
import { useState } from "react";
import { createContext } from "react";

const ContextoConteoServicios = createContext({
    conteoServicios: 0,
    setConteoServicios: () => {}
})

export const ProveedorContextoConteoServicios = ({children}) => {
    const [conteoServicios, _setConteoServicios] = useState(0);

    const setConteoServicios = (data) => {
        _setConteoServicios(data)
    }

    return (
        <ContextoConteoServicios.Provider value={{conteoServicios, setConteoServicios}}>
            {children}
        </ContextoConteoServicios.Provider>
    )
}

export const UsarContextoConteoServicios = () => useContext(ContextoConteoServicios)
