import { useContext } from "react";
import { useState } from "react";
import { createContext } from "react";

const ContextoConteoCuentas = createContext({
    conteoCuentas: 0,
    setConteoCuentas: () => {}
})

export const ProveedorContextoConteoCuentas = ({children}) => {
    const [conteoCuentas, _setConteoCuentas] = useState(0);

    const setConteoCuentas = (data) => {
        _setConteoCuentas(data)
    }

    return (
        <ContextoConteoCuentas.Provider value={{conteoCuentas, setConteoCuentas}}>
            {children}
        </ContextoConteoCuentas.Provider>
    )
}

export const UsarContextoConteoCuentas = () => useContext(ContextoConteoCuentas)
