import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {Buffer} from 'buffer';
import { UsarContextoConteoCuentas } from '../contextos/ContextoConteoCuentas';

const ListaCuentas = (props) => {
    const [cuentas, setCuentas] = useState([]);
    const {conteoCuentas} = UsarContextoConteoCuentas()
    const tokenAutorizacion = Buffer.from(`${process.env.REACT_APP_API_USR_KEY}:${process.env.REACT_APP_API_USR_PASS}`).toString('base64');

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}cuentas/`, {
            headers: {
                'Authorization': `Basic ${tokenAutorizacion}`
            }})
            .then(function (response) {
                setCuentas(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, [conteoCuentas, tokenAutorizacion]);

    function handleChange(e) {
        var textoFiltro = e.currentTarget.value;
        textoFiltro = textoFiltro.toUpperCase();

        var tabla = document.getElementById("tablaCuentas");
        var filasTabla = tabla.getElementsByTagName("tr");
        for (var i=0; i<filasTabla.length; i++) {
            var columnas = filasTabla[i].getElementsByTagName("td");
            for (var j=0; j<columnas.length; j++) {
                if(columnas[j]) {
                    var valorColumna = columnas[j].textContent || columnas[j].innerText;
                    valorColumna = valorColumna.toUpperCase();
                    if (valorColumna.indexOf(textoFiltro) > -1) {
                        filasTabla[i].style.display = "";
                        break;
                    } else {
                        filasTabla[i].style.display = "none";
                    }
                }
            }
        }
    }

    return (
        <>
            <input type="text" id="txtBuscarCuenta" name="txtBuscarCuenta" placeholder="Filtra las cuentas" style={{ width: '90vw', border: '1px solid darkgray', borderRadius: '5px', fontSize: '1.5rem'  }} onChange={handleChange} />

            <h3>{props.children}</h3>

            <table id="tablaCuentas" style={{border:'1px solid black', borderCollapse: 'collapse', marginTop: '2rem', marginLeft: 'auto', marginRight: 'auto', width: '70vw'}}>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Servicio</th>
                        <th>Descripción</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                {
                    cuentas.map((elemento) => {
                        return (            
                            <tr key={elemento.id}>
                                <td>{elemento.id}</td>
                                <td>{elemento.servicio.nombre}</td>
                                <td>{elemento.descripcion}</td>
                                <td>{elemento.estadocuenta.nombre}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </>
    );
}

export default ListaCuentas;