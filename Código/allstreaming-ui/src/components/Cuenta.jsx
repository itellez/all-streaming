import React from 'react';

const Estado = (props) => {
    var colorTexto = '';
    switch (props.children) {
        case 'Disponible':
            colorTexto = 'green';
            break;
        case 'En Uso':
            colorTexto = 'gold';
            break;
        case 'Bloqueada':
            colorTexto = 'gray';
            break;
        default:
            colorTexto = 'red';
            break;
    }

    return (
        <h4 style={{color:colorTexto, fontWeight:'bold', textAlign: 'right'}}>{props.children}</h4>
    );
}

export default function Cuenta(props) {

    return (
        <div id={`account_${props.id}`} style={{display:'flex', border:'2px solid darkblue', padding: '1rem', width: '15vw'}}>
            <div style={{marginRight: '1rem'}}>
                <h3>{props.titulo}</h3>
                <h6>{props.texto}</h6>
            </div>
            <div style={{}}>
                <Estado>{props.estado}</Estado>
            </div>
        </div>
    );
};