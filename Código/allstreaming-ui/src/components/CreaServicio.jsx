import axios from 'axios';
import React from 'react';
import {Buffer} from 'buffer';
import { UsarContextoConteoServicios } from '../contextos/ContextoConteoServicios';

const CreaServicio = (props) => {
    const {conteoServicios, setConteoServicios} = UsarContextoConteoServicios()
    const tokenAutorizacion = Buffer.from(`${process.env.REACT_APP_API_USR_KEY}:${process.env.REACT_APP_API_USR_PASS}`).toString('base64');

    function handleSubmit(e) {
        e.preventDefault();

        const url = `${process.env.REACT_APP_API_URL}servicios/`;
        const servicio = {
            'nombre': document.getElementById('txtNombreServicio').value,
            'url': document.getElementById('txtUrlServicio').value,
            'preciomensual': document.getElementById('txtPrecioMensual').value
        };

        axios({
            method: 'post',
            url: url, 
            data: servicio,
            headers: {
                'Authorization': `Basic ${tokenAutorizacion}`
            }
        })
            .then(function(response) {
                alert('El servicio ' + document.getElementById('txtNombreServicio').value + ' se creó con éxito');
                document.getElementById('formServicio').reset();
                setConteoServicios(conteoServicios+1);
            })
            .catch(function(error) {
                console.log(error);
            })
    }

    return (
        <div id="divCrearServicio">
            <h2>Crea un Nuevo Servicio</h2>
            <form id="formServicio" method="post" onSubmit={handleSubmit}>
                <input type="text" id="txtNombreServicio" name="txtNombreServicio" style={{width:'100%'}} placeholder="Nombre del servicio" required />
                <input type="url" id="txtUrlServicio" name="txtUrlServicio" style={{width:'100%'}} placeholder="URL del servicio" required />
                <input type="number" id="txtPrecioMensual" name="txtPrecioMensual" style={{width:'100%'}} placeholder="Precio mensual del servicio" required />
                <div style={{marginTop: '1rem'}}>
                    <button type="submit" style={{backgroundColor: 'green', fontSize: '1rem', padding: '0.5rem', borderRadius: '0.5rem', color: 'white', marginRight: '0.5rem'}}>Crear</button>
                    <button type="reset" style={{backgroundColor: 'gold', fontSize: '1rem', padding: '0.5rem', borderRadius: '0.5rem', color: 'black', marginLeft: '0.5rem'}}>Reiniciar</button>
                </div>
            </form>
        </div>
    );
}

export default CreaServicio;