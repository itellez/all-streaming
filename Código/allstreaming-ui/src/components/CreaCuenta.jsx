import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {Buffer} from 'buffer';
import { UsarContextoConteoCuentas } from '../contextos/ContextoConteoCuentas';
import { UsarContextoConteoServicios } from '../contextos/ContextoConteoServicios';

const CreaCuenta = (props) => {
    const [servicios, setServicios] = useState([]);
    const {conteoServicios} = UsarContextoConteoServicios()
    const {conteoCuentas, setConteoCuentas} = UsarContextoConteoCuentas()
    const tokenAutorizacion = Buffer.from(`${process.env.REACT_APP_API_USR_KEY}:${process.env.REACT_APP_API_USR_PASS}`).toString('base64');

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}servicios/`, {
            headers: {
                'Authorization': `Basic ${tokenAutorizacion}`
            }})
        .then(function(response) {
            setServicios(response.data);
        })
        .catch(function(error) {
            console.log(error);
        })
    }, [conteoServicios, tokenAutorizacion]);

    function handleSubmit(e) {
        e.preventDefault();

        const clave = document.getElementById('txtClaveCuenta').value;
        const confirmarClave = document.getElementById('txtConfirmarCuenta').value;

        if (clave !== confirmarClave) {
            alert('Las claves no son iguales. Deben ser idénticas.');
        }
        else {
            const idServicio = document.getElementById('selServicios').value;
            const url = `${process.env.REACT_APP_API_URL}cuentas/${idServicio}`;
            const cuenta = {
                'usuario': document.getElementById('txtEmailCuenta').value,
                'clave': document.getElementById('txtClaveCuenta').value,
                'descripcion': document.getElementById('taDescripcionCuenta').value
            };
    
            axios({
                method: 'post',
                url: url, 
                data: cuenta,
                headers: {
                    'Authorization': `Basic ${tokenAutorizacion}`
                }
            })
                .then(function(response) {
                    alert('La cuenta ' + document.getElementById('txtNombreServicio').value + ' fue creada exitosamente.');
                    document.getElementById('formCuenta').reset();
                    setConteoCuentas(conteoCuentas+1);
                })
                .catch(function(error) {
                    console.log(error);
                })
        }
    }

    return (
        <div id="divCrearCuenta">
            <h2>Crea una Cuenta Nueva</h2>
            <form id="formCuenta" method="post" onSubmit={handleSubmit}>
                <select id="selServicios" style={{}} required>
                    <option value="">Indica un servicio para la cuenta</option>
                    {
                        servicios.map((servicio) => {
                            return (
                                <option key={servicio.id} value={servicio.id}>{servicio.nombre}</option>
                            )
                        })
                    }
                </select>
                <input type="email" id="txtEmailCuenta" name="txtEmailCuenta" style={{width:'100%'}} placeholder="Email para la cuenta" maxLength={45} required />
                <div style={{display:'flex'}}>
                    <input type="password" id="txtClaveCuenta" name="txtClaveCuenta" style={{width:'50%'}} placeholder="Clave de la cuenta" maxLength={45} required />
                    <input type="password" id="txtConfirmarCuenta" name="txtConfirmarCuenta" style={{width:'50%'}} placeholder="Confirmar clave" maxLength={45} required />
                </div>
                <textarea id="taDescripcionCuenta" style={{width:'100%'}} placeholder="Describe la cuenta" maxLength={120} required></textarea>
                <div style={{marginTop: '1rem'}}>
                    <button type="submit" style={{backgroundColor: 'green', fontSize: '1rem', padding: '0.5rem', borderRadius: '0.5rem', color: 'white', marginRight: '0.5rem'}}>Crear</button>
                    <button type="reset" style={{backgroundColor: 'gold', fontSize: '1rem', padding: '0.5rem', borderRadius: '0.5rem', color: 'black', marginLeft: '0.5rem'}}>Reiniciar</button>
                </div>
            </form>
        </div>
    );
}

export default CreaCuenta;