import './App.css';
import CreaCuenta from './components/CreaCuenta';
import CreaServicio from './components/CreaServicio';
import ListaCuentas from './components/ListaCuentas';
import { ProveedorContextoConteoCuentas } from './contextos/ContextoConteoCuentas';
import { ProveedorContextoConteoServicios } from './contextos/ContextoConteoServicios';

function App() {
	document.title = 'All Streaming | Entrevista Rule 1';

	return (
		<>
            <ProveedorContextoConteoCuentas>
                <ProveedorContextoConteoServicios>
                    <div className="App" style={{ padding: '2rem' }}>
                        <header>
                            <h1 style={{fontSize: '3rem'}}>All Streaming</h1>
                        </header>

                        <ListaCuentas>
                            Mis cuentas de Streaming
                        </ListaCuentas>

                        <hr style={{ marginTop: '2rem'}} />
                        
                        <div style={{ display:'flex', width: '100%'}}>
                            <div className="split left">
                                <CreaCuenta />
                            </div>
                            <div className="split right">
                                <CreaServicio />
                            </div>
                        </div>
                    </div>
                </ProveedorContextoConteoServicios>
            </ProveedorContextoConteoCuentas>
		</>
	);
}

export default App;
