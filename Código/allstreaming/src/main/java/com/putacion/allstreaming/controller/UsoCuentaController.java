package com.putacion.allstreaming.controller;

import com.putacion.allstreaming.model.UsoCuenta;
import com.putacion.allstreaming.service.IUsoCuentaService;
import com.putacion.allstreaming.utils.CalculoTiempo;
import com.putacion.allstreaming.utils.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/usos")
public class UsoCuentaController {

    @Autowired
    private IUsoCuentaService usoCuentaService;

    @PostMapping(path = "/arriendo/{idArriendoCuenta}/usuario/{idUsuario}/cuenta/{idCuenta}/tiempo/{segundosDeUso}", produces = "application/json")
    public ResponseEntity registrarUso(@PathVariable Integer idArriendoCuenta, @PathVariable Integer idUsuario, @PathVariable Integer idCuenta, @PathVariable Integer segundosDeUso) {
        try {
            Optional<UsoCuenta> usoCuenta = usoCuentaService.agregarUsoCuenta(idArriendoCuenta, idUsuario, idCuenta, segundosDeUso);
            if (usoCuenta.isEmpty())
                return ResponseHandler.generateResponse("No fue posible registrar el uso de cuenta.", HttpStatus.BAD_REQUEST);

            return ResponseHandler.generateResponse("Se registró exitosamente el uso de cuenta.", HttpStatus.CREATED);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar registrar uso de cuenta (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/arriendo/{idArriendoCuenta}/usuario/{idUsuario}/saldo", produces = "application/json")
    public ResponseEntity obtenerSaldo(@PathVariable Integer idArriendoCuenta, @PathVariable Integer idUsuario) {
        try {
            Integer saldoUso = usoCuentaService.obtenerSaldoUsoCuenta(idArriendoCuenta, idUsuario);
            CalculoTiempo calculoTiempo = new CalculoTiempo(saldoUso);

            return new ResponseEntity(calculoTiempo, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar obtener el saldo de uso (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
