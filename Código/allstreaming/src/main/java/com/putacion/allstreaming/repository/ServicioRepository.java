package com.putacion.allstreaming.repository;

import com.putacion.allstreaming.model.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServicioRepository extends JpaRepository<Servicio, Integer> {
    public boolean existsServicioByNombre(String nombre);
    public Optional<Servicio> findByNombreLike(String nombre);
}
