package com.putacion.allstreaming.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.util.DigestUtils;

import javax.persistence.*;
import java.util.List;

@Entity
public class Usuario extends Entidad {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String email;
    private String nombre;
//    @JsonIgnore
    private String clave;
    private Boolean esconsumidor;

//    @OneToMany
//    private List<UsoCuenta> usosCuenta;

    public Usuario() {
        super();
    }

    public Usuario(Integer idUsuario) {
        super(idUsuario);
    }

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRolUsuario() {
        return "Usuario";
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
//        this.clave = DigestUtils.md5DigestAsHex(clave.getBytes());
        this.clave = clave;
    }

//    public List<UsoCuenta> getUsosCuenta() {
//        return usosCuenta;
//    }
//
//    public void setUsosCuenta(List<UsoCuenta> usosCuenta) {
//        this.usosCuenta = usosCuenta;
//    }

    public Boolean getEsconsumidor() {
        return esconsumidor;
    }

    public void setEsconsumidor(Boolean esconsumidor) {
        this.esconsumidor = esconsumidor;
    }
}
