package com.putacion.allstreaming.service;

import com.putacion.allstreaming.model.ArriendoCuenta;

import java.util.Optional;

public interface IArriendoCuentaService {
    public Optional<ArriendoCuenta> obtenerArriendoCuenta(Integer idArriendoCuenta) throws Exception;
    public Optional<ArriendoCuenta> registrarArriendoDias(Integer idCuenta, Integer idUsuario, Integer dias) throws Exception;
    public Optional<ArriendoCuenta> registrarArriendoHoras(Integer idCuenta, Integer idUsuario, Integer horas) throws Exception;
    public boolean eliminarArriendoCuenta(Integer idArriendoCuenta);
}
