package com.putacion.allstreaming.repository;

import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CuentaRepository extends JpaRepository<Cuenta, Integer> {
    public boolean existsCuentaByServicioAndUsuario(Servicio servicio, String usuario);
    public List<Cuenta> findAllByServicio(Servicio servicio);
}
