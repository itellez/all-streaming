package com.putacion.allstreaming.service;

import com.putacion.allstreaming.model.UsoCuenta;

import java.util.Optional;

public interface IUsoCuentaService {
    public Optional<UsoCuenta> agregarUsoCuenta(Integer idArriendoCuenta, Integer idUsuario, Integer idCuenta, Integer segundosDeUso) throws Exception;
    public Integer obtenerSaldoUsoCuenta(Integer idArriendoCuenta, Integer idUsuario);
}
