package com.putacion.allstreaming.service;

import com.putacion.allstreaming.model.Servicio;

import java.util.List;
import java.util.Optional;

public interface IServicioService {
    public Optional<Servicio> obtenerServicio(Integer id);
    public List<Servicio> listarServicios() throws Exception;
    public Optional<Servicio> guardarServicio(Servicio servicio) throws Exception;
    public boolean quitarServicio(Integer id);
    public boolean eliminarServicio(Integer id);
}
