package com.putacion.allstreaming.controller;

import com.putacion.allstreaming.model.Usuario;
import com.putacion.allstreaming.repository.UsuarioRepository;
import com.putacion.allstreaming.utils.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UsuarioRepository repositorioUsuarios;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(path = "/login", produces = "application/json")
    public ResponseEntity login(@RequestBody Usuario user) throws Exception {

        Authentication authObject = null;
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getClave());
            authObject = authenticationManager.authenticate(token);
            if (authObject != null) {
                Usuario usuario = repositorioUsuarios.findByEmail(user.getEmail()).get();
                if (usuario.getEsconsumidor()) {
                    return ResponseHandler.generateResponse("Este usuario no tiene permisos para usar la API.", HttpStatus.BAD_REQUEST);
                }
                SecurityContextHolder.getContext().setAuthentication(authObject);
                return ResponseHandler.generateResponse("¡Login correcto!", HttpStatus.OK);
            }
            else {
                return ResponseHandler.generateResponse("No fue posible validar el login.", HttpStatus.BAD_REQUEST);
            }
        } catch (BadCredentialsException e) {
            throw new Exception("Credenciales incorrectas o inexistentes");
        }
    }

     @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity register(@RequestBody Usuario user) throws Exception {
        try {
            if (repositorioUsuarios.existsUsuarioByEmail(user.getEmail())) {
                return ResponseHandler.generateResponse("¡Ese email ya está registrado!", HttpStatus.BAD_REQUEST);
            }

            Usuario nuevoUsuario = new Usuario();
            nuevoUsuario.setEmail(user.getEmail());
            nuevoUsuario.setNombre(user.getNombre());
            nuevoUsuario.setClave(passwordEncoder.encode(user.getClave()));
            nuevoUsuario.setEsconsumidor(user.getEsconsumidor());
            repositorioUsuarios.save(nuevoUsuario);
            if (nuevoUsuario != null)
                return new ResponseEntity<HttpStatus>(HttpStatus.OK);

            return ResponseHandler.generateResponse("No se pudo crear el nuevo usuario.", HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            throw ex;
        }
     }

}
