package com.putacion.allstreaming.repository;

import com.putacion.allstreaming.model.ArriendoCuenta;
import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArriendoCuentaRepository extends JpaRepository<ArriendoCuenta, Integer> {
    public boolean existsArriendoCuentaByUsuarioAndCuenta(Usuario usuario, Cuenta cuenta);
    public Optional<ArriendoCuenta> findByUsuarioAndCuenta(Usuario usuario, Cuenta cuenta);
}
