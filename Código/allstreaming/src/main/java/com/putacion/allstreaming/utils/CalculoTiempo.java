package com.putacion.allstreaming.utils;

public class CalculoTiempo {
    private Integer tiempoCompleto;
    private Integer diasTiempo = 0;
    private Integer horasTiempo = 0;
    private Integer minutosTiempo = 0;
    private Integer segundosTiempo = 0;
    private Integer segundosPorDia = 86400;
    private Integer segundosPorHora = 3600;
    private Integer segundosPorMinuto = 60;

    public CalculoTiempo(Integer tiempoEnSegundos)
    {
        this.tiempoCompleto = tiempoEnSegundos;
        this.calcular(tiempoEnSegundos);
    }

    private void calcular(Integer tiempoEnSegundos)
    {
        Integer tiempoTotal = tiempoEnSegundos;

        tiempoTotal = this.obtenerDias(tiempoTotal);
        tiempoTotal = this.obtenerHoras(tiempoTotal);
        tiempoTotal = this.obtenerMinutos(tiempoTotal);
        this.segundosTiempo = tiempoTotal;
    }

    private Integer obtenerDias(Integer tiempoEnSegundos)
    {
        if (tiempoEnSegundos >= this.segundosPorDia) {
            diasTiempo = tiempoEnSegundos / segundosPorDia;
        }

        return tiempoEnSegundos - (diasTiempo * segundosPorDia);
    }

    private Integer obtenerHoras(Integer tiempoEnSegundos)
    {
        if (tiempoEnSegundos >= this.segundosPorHora) {
            horasTiempo = tiempoEnSegundos / segundosPorHora;
        }

        return tiempoEnSegundos - (horasTiempo * segundosPorHora);
    }

    private Integer obtenerMinutos(Integer tiempoEnSegundos)
    {
        if (tiempoEnSegundos >= this.segundosPorMinuto) {
            minutosTiempo = tiempoEnSegundos / segundosPorMinuto;
        }

        return tiempoEnSegundos - (minutosTiempo * segundosPorMinuto);
    }

    public Integer getTiempoCompleto() {
        return tiempoCompleto;
    }

    public void setTiempoCompleto(Integer tiempoCompleto) {
        this.tiempoCompleto = tiempoCompleto;
        this.calcular(tiempoCompleto);
    }

    public Integer getDiasTiempo() {
        return diasTiempo;
    }

    public Integer getHorasTiempo() {
        return horasTiempo;
    }

    public Integer getMinutosTiempo() {
        return minutosTiempo;
    }

    public Integer getSegundosTiempo() {
        return segundosTiempo;
    }
}
