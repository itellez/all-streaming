package com.putacion.allstreaming.repository;

import com.putacion.allstreaming.domain.Enumeraciones;
import com.putacion.allstreaming.model.EstadoCuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstadoCuentaRepository extends JpaRepository<EstadoCuenta, Integer> {
    Optional<EstadoCuenta> findByNombre(String nombre);

    Optional<EstadoCuenta> findById(Enumeraciones.Estado_Cuenta id);
}
