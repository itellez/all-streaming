package com.putacion.allstreaming.service;

import com.putacion.allstreaming.domain.Enumeraciones;
import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.Servicio;

import java.util.List;
import java.util.Optional;

public interface ICuentaService {
    public Optional<Cuenta> obtenerCuenta(Integer id);
    public Optional<Servicio> obtenerServicioCuenta(Integer id) throws Exception;
    public Optional<Cuenta> guardarCuenta(Cuenta cuenta, Integer idServicio, Enumeraciones.Estado_Cuenta estadoCuenta) throws Exception;
    public boolean quitarCuenta(Integer id);
    public boolean eliminarCuenta(Integer id);
    public Optional<Cuenta> marcarEstado(Enumeraciones.Estado_Cuenta estado, Integer id) throws Exception;
    public List<Cuenta> buscarCuentasDisponibles(String servicio) throws Exception;
    public List<Cuenta> listarCuentas() throws Exception;
}
