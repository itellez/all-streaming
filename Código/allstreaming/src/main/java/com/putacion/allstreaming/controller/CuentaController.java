package com.putacion.allstreaming.controller;

import com.putacion.allstreaming.domain.Enumeraciones;
import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.Servicio;
import com.putacion.allstreaming.service.ICuentaService;
import com.putacion.allstreaming.utils.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/cuentas")
public class CuentaController {

    @Autowired
    private ICuentaService cuentaService;

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity obtenerCuenta(@PathVariable Integer id) {
        try {
            Optional<Cuenta> cuenta = cuentaService.obtenerCuenta(id);
            if (cuenta.isEmpty())
                return ResponseHandler.generateResponse("No se encontró la cuenta indicada.", HttpStatus.NOT_FOUND);

            return new ResponseEntity(cuenta.get(), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar obtener el servicio (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/{id}/servicio", produces = "application/json")
    public ResponseEntity obtenerServicioCuenta(@PathVariable Integer id) {
        try {
            Optional<Servicio> servicio = cuentaService.obtenerServicioCuenta(id);
            if (servicio.isEmpty())
                return ResponseHandler.generateResponse("No se encontró el servicio para la cuenta.", HttpStatus.NOT_FOUND);

            return new ResponseEntity(servicio.get(), HttpStatus.OK);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar obtener el servicio (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/{idServicio}", produces = "application/json")
    public ResponseEntity crearCuenta(@RequestBody Cuenta cuenta, @PathVariable Integer idServicio) {
        try {
            Optional<Cuenta> cuentaGuardada = cuentaService.guardarCuenta(cuenta, idServicio, Enumeraciones.Estado_Cuenta.DISPONIBLE);
            if (cuentaGuardada.isEmpty())
                return ResponseHandler.generateResponse("No se pudo crear la nueva cuenta.", HttpStatus.BAD_REQUEST);

            return new ResponseEntity(cuentaGuardada.get(), HttpStatus.CREATED);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar crear la cuenta (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity eliminarCuenta(@PathVariable Integer id) {
        try {
            if (cuentaService.eliminarCuenta(id))
                return ResponseHandler.generateResponse("Cuenta eliminada sin problemas.", HttpStatus.OK);

            return ResponseHandler.generateResponse("No se pudo eliminar la cuenta indicada.", HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar eliminar la cuenta (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "/{id}/disponible", produces = "application/json")
    public ResponseEntity marcarDisponible(@PathVariable Integer id) {
        try {
            Optional<Cuenta> cuentaGuardada = cuentaService.marcarEstado(Enumeraciones.Estado_Cuenta.DISPONIBLE, id);
            if (!cuentaGuardada.isEmpty())
                return ResponseHandler.generateResponse("Se cambió el estado de la cuenta a Disponible.", HttpStatus.OK);

            return ResponseHandler.generateResponse("No fue posible guardar el cambio de estado.", HttpStatus.BAD_REQUEST);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar cambiar el estado de la cuenta (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "/{id}/enuso", produces = "application/json")
    public ResponseEntity marcarEnUso(@PathVariable Integer id) {
        try {
            Optional<Cuenta> cuentaGuardada = cuentaService.marcarEstado(Enumeraciones.Estado_Cuenta.EN_USO, id);
            if (!cuentaGuardada.isEmpty())
                return ResponseHandler.generateResponse("Se cambió el estado de la cuenta a En Uso.", HttpStatus.OK);

            return ResponseHandler.generateResponse("No fue posible guardar el cambio de estado.", HttpStatus.BAD_REQUEST);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar cambiar el estado de la cuenta (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "/{id}/bloqueada", produces = "application/json")
    public ResponseEntity marcarBloqueada(@PathVariable Integer id) {
        try {
            Optional<Cuenta> cuentaGuardada = cuentaService.marcarEstado(Enumeraciones.Estado_Cuenta.BLOQUEADA, id);
            if (!cuentaGuardada.isEmpty())
                return ResponseHandler.generateResponse("Se cambió el estado de la cuenta a Bloqueada.", HttpStatus.OK);

            return ResponseHandler.generateResponse("No fue posible guardar el cambio de estado.", HttpStatus.BAD_REQUEST);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al cambiar el estado de la cuenta (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/buscar/{servicio}", produces = "application/json")
    public ResponseEntity buscarCuentas(@PathVariable String servicio) {
        try {
            List<Cuenta> listaCuentas = cuentaService.buscarCuentasDisponibles(servicio);
            if (listaCuentas != null && listaCuentas.size() > 0) {
                return new ResponseEntity(listaCuentas, HttpStatus.OK);
            }

            return ResponseHandler.generateResponse("No se encontraron cuentas disponibles para el servicio indicado.", HttpStatus.BAD_REQUEST);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error buscando las cuentas (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/", produces = "application/json")
    public ResponseEntity listarCuentas() {
        try {
            List<Cuenta> listaCuentas = cuentaService.listarCuentas();
            if (Objects.isNull(listaCuentas) || listaCuentas.size() <= 0)
                return ResponseHandler.generateResponse("No se encontraron cuentas en el sistema.", HttpStatus.BAD_REQUEST);

            return new ResponseEntity(listaCuentas, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error buscando las cuentas (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
