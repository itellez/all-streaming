package com.putacion.allstreaming.repository;

import com.putacion.allstreaming.model.ArriendoCuenta;
import com.putacion.allstreaming.model.UsoCuenta;
import com.putacion.allstreaming.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsoCuentaRepository extends JpaRepository<UsoCuenta, Integer> {
//    public List<UsoCuenta> findByUsuarioAndArriendocuenta(Usuario usuario, ArriendoCuenta arriendocuenta);
    public List<UsoCuenta> findByArriendocuenta(ArriendoCuenta arriendocuenta);
}
