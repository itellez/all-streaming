package com.putacion.allstreaming.service;

import com.putacion.allstreaming.model.ArriendoCuenta;
import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.UsoCuenta;
import com.putacion.allstreaming.model.Usuario;
import com.putacion.allstreaming.repository.ArriendoCuentaRepository;
import com.putacion.allstreaming.repository.CuentaRepository;
import com.putacion.allstreaming.repository.UsoCuentaRepository;
import com.putacion.allstreaming.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsoCuentaService implements IUsoCuentaService {

    @Autowired
    private UsoCuentaRepository usoCuentaRepository;
    @Autowired
    private CuentaRepository cuentaRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private ArriendoCuentaRepository arriendoCuentaRepository;

    @Override
    public Optional<UsoCuenta> agregarUsoCuenta(Integer idArriendoCuenta, Integer idUsuario, Integer idCuenta, Integer segundosDeUso) throws Exception {
        Integer saldoUsoCuenta = this.calcularSaldoUsoCuenta(idArriendoCuenta, idUsuario);
        if (saldoUsoCuenta <= 0 || saldoUsoCuenta <= segundosDeUso) {
            if (saldoUsoCuenta > 0)
                throw new Exception("Al arriendo de esta cuenta le queda menos tiempo del que se intenta registrar.");
            else
                throw new Exception("El arriendo de esta cuenta ya agotó su tiempo.");
        }

        Optional<Cuenta> cuenta = cuentaRepository.findById(idCuenta);
        Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
        Optional<ArriendoCuenta> arriendocuenta = arriendoCuentaRepository.findById(idArriendoCuenta);
        if (cuenta.isEmpty() || usuario.isEmpty() || arriendocuenta.isEmpty()) {
            throw new Exception("No se pudo identificar uno de los componentes de la cuenta a usar.");
        }

        Optional<ArriendoCuenta> arriendoSolicitado = arriendoCuentaRepository.findByUsuarioAndCuenta(usuario.get(), cuenta.get());
        if (arriendoSolicitado.isEmpty()) {
            throw new NullPointerException("No se encontró un arriendo de la cuenta indicada para el usuario " + usuario.get().getNombre());
        }

        if (arriendoSolicitado.get().getId() != idArriendoCuenta) {
            throw new Exception("El arriendo solicitado no coincide con el usuario y la cuenta indicados");
        }

        UsoCuenta usoCuenta = new UsoCuenta();
        usoCuenta.setCuenta(cuenta.get());
        usoCuenta.setUsuario(usuario.get());
        usoCuenta.setArriendocuenta(arriendocuenta.get());
        usoCuenta.setTiempouso(segundosDeUso);

        UsoCuenta usoCuentaGuardado = usoCuentaRepository.save(usoCuenta);
        return Optional.of(usoCuentaGuardado);
    }

    @Override
    public Integer obtenerSaldoUsoCuenta(Integer idArriendoCuenta, Integer idUsuario) {
        return this.calcularSaldoUsoCuenta(idArriendoCuenta, idUsuario);
    }

    private Integer calcularSaldoUsoCuenta(Integer idArriendoCuenta, Integer idUsuario) {
        Optional<ArriendoCuenta> arriendoCuenta = arriendoCuentaRepository.findById(idArriendoCuenta);
        Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);

        if (!arriendoCuenta.isEmpty() && !usuario.isEmpty()) {
            return -1;
        }

        Integer diasArriendo = arriendoCuenta.get().getDias();
        Integer horasArriendo = arriendoCuenta.get().getHoras();

        diasArriendo *= 24 * 3600;    // en segundos
        horasArriendo *= 3600;

        Integer totalSegundosArriendo = diasArriendo + horasArriendo;
        Integer totalSegundosUsados = 0;

        List<UsoCuenta> usoCuentaList = usoCuentaRepository.findByArriendocuenta(arriendoCuenta.get());
        for (UsoCuenta u : usoCuentaList) {
            totalSegundosUsados += u.getTiempouso();
        }

        return totalSegundosArriendo - totalSegundosUsados;
    }
}
