package com.putacion.allstreaming.repository;

import com.putacion.allstreaming.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    Boolean existsUsuarioByEmail(String email);

    Optional<Usuario> findByEmail(String email);
}
