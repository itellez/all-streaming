package com.putacion.allstreaming.service;

import com.putacion.allstreaming.domain.Enumeraciones;
import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.EstadoCuenta;
import com.putacion.allstreaming.model.Servicio;
import com.putacion.allstreaming.repository.CuentaRepository;
import com.putacion.allstreaming.repository.EstadoCuentaRepository;
import com.putacion.allstreaming.repository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CuentaService implements ICuentaService {

    @Autowired
    private CuentaRepository cuentaRepository;
    @Autowired
    private EstadoCuentaRepository estadoCuentaRepository;
    @Autowired
    private ServicioRepository servicioRepository;

    @Override
    public Optional<Cuenta> obtenerCuenta(Integer id) {
        Optional<Cuenta> cuenta = cuentaRepository.findById(id);

        return cuenta;
    }

    @Override
    public Optional<Servicio> obtenerServicioCuenta(Integer id) throws Exception {
        Optional<Cuenta> cuenta = cuentaRepository.findById(id);
        if (cuenta.isEmpty())
            throw new NullPointerException("No se encontró la cuenta especificada.");

        return Optional.of(cuenta.get().getServicio());
    }

    @Override
    public Optional<Cuenta> guardarCuenta(Cuenta cuenta, Integer idServicio, Enumeraciones.Estado_Cuenta estadoCuenta) throws Exception {
        Optional<Servicio> servicio = servicioRepository.findById(idServicio);
        if (servicio.isEmpty())
            throw new NullPointerException("No se encontró el servicio indicado");

        if (cuentaRepository.existsCuentaByServicioAndUsuario(servicio.get(), cuenta.getUsuario()))
            throw new Exception("La cuenta solicitada ya existe");

        Optional<EstadoCuenta> estadoCuentaObtenido = estadoCuentaRepository.findByNombre(estadoCuenta.toString());
        if (estadoCuentaObtenido.isEmpty())
            throw new Exception("El estado inicial de la cuenta no es válido");

        Cuenta nuevaCuenta = new Cuenta();
        nuevaCuenta.setServicio(servicio.get());
        nuevaCuenta.setEstadocuenta(estadoCuentaObtenido.get());
        nuevaCuenta.setUsuario(cuenta.getUsuario());
        nuevaCuenta.setClave(cuenta.getClave());
        nuevaCuenta.setDescripcion(cuenta.getDescripcion());

        Cuenta cuentaGuardada = cuentaRepository.save(nuevaCuenta);
        return Optional.of(cuentaGuardada);
    }

    @Override
    public boolean eliminarCuenta(Integer id) {
        Optional<Cuenta> cuenta = cuentaRepository.findById(id);
        if (cuenta.isEmpty())
            return false;

        cuentaRepository.delete(cuenta.get());
        return true;
    }

    @Override
    public boolean quitarCuenta(Integer id) {
        Optional<Cuenta> cuenta = cuentaRepository.findById(id);
        if (cuenta.isEmpty())
            return false;

        cuenta.get().setCambiosAuditoria();
        cuenta.get().setVigente(false);

        Cuenta cuentaGuardada = cuentaRepository.save(cuenta.get());
        return (cuentaGuardada != null);
    }

    @Override
    public Optional<Cuenta> marcarEstado(Enumeraciones.Estado_Cuenta estado, Integer id) throws Exception {
        Optional<EstadoCuenta> nuevoEstado = estadoCuentaRepository.findById(estado);
        Optional<Cuenta> cuenta = cuentaRepository.findById(id);
        if (nuevoEstado.isEmpty() || cuenta.isEmpty())
            throw new NullPointerException("No se pudieron encontrar algunos de los componentes de la cuenta a modificar.");

        cuenta.get().setEstadocuenta(nuevoEstado.get());
        cuenta.get().setCambiosAuditoria();

        return Optional.of(cuentaRepository.save(cuenta.get()));
    }

    @Override
    public List<Cuenta> buscarCuentasDisponibles(String nombreServicio) throws Exception {
        Optional<Servicio> servicio = servicioRepository.findByNombreLike(nombreServicio);
        if (servicio.isEmpty())
            throw new NullPointerException("No se encontró un servicio bajo ese nombre");

        List<Cuenta> cuentaList = cuentaRepository.findAllByServicio(servicio.get());
        return cuentaList;
    }

    @Override
    public List<Cuenta> listarCuentas() throws Exception {
        List<Cuenta> cuentaList = cuentaRepository.findAll();
        if (cuentaList == null || cuentaList.size() <= 0)
            throw new Exception("No se encontraron cuentas en el sistema.");

        return cuentaList;
    }
}
