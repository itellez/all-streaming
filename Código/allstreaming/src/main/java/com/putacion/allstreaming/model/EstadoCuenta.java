package com.putacion.allstreaming.model;

import javax.persistence.*;

@Entity(name="estadocuenta")
public class EstadoCuenta extends Entidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nombre;

//    @OneToOne
//    private Cuenta cuenta;

    public EstadoCuenta() {
        super();
    }

    public EstadoCuenta(Integer idUsuario) {
        super(idUsuario);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public Cuenta getCuenta() {
//        return cuenta;
//    }
//
//    public void setCuenta(Cuenta cuenta) {
//        this.cuenta = cuenta;
//    }
}
