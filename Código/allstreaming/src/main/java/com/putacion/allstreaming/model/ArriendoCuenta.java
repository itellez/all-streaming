package com.putacion.allstreaming.model;

import javax.persistence.*;

@Entity(name="arriendocuenta")
public class ArriendoCuenta extends Entidad {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Integer horas;
    private Integer dias;

    @ManyToOne
    private Usuario usuario;
    @ManyToOne
    private Cuenta cuenta;

    public ArriendoCuenta() { super(); }

    public Integer getHoras() {
        return horas;
    }

    public void setHoras(Integer horas) {
        this.horas = horas;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getId() {
        return id;
    }
}
