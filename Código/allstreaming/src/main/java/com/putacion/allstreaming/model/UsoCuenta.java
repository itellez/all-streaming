package com.putacion.allstreaming.model;

import javax.persistence.*;

@Entity(name="usocuenta")
public class UsoCuenta extends Entidad {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Integer tiempouso;

    @ManyToOne
    @JoinColumn(name = "arriendocuenta_usuario_id")
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "arriendocuenta_cuenta_id")
    private Cuenta cuenta;
    @ManyToOne
    private ArriendoCuenta arriendocuenta;

    public UsoCuenta() {
        super();
    }

    public UsoCuenta(Integer idUsuario) {
        super(idUsuario);
    }

    public Integer getId() {
        return id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getTiempouso() {
        return tiempouso;
    }

    public void setTiempouso(Integer tiempouso) {
        this.tiempouso = tiempouso;
    }

    public ArriendoCuenta getArriendocuenta() {
        return arriendocuenta;
    }

    public void setArriendocuenta(ArriendoCuenta arriendocuenta) {
        this.arriendocuenta = arriendocuenta;
    }
}
