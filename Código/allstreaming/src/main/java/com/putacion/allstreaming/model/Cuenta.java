package com.putacion.allstreaming.model;

import javax.persistence.*;

@Entity
public class Cuenta extends Entidad {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String usuario;
    private String clave;
    private String descripcion;

    @OneToOne
    @JoinColumn(name = "servicio_id")
    private Servicio servicio;
    @OneToOne
    @JoinColumn(name = "estadocuenta_id")
    private EstadoCuenta estadocuenta;

    public Cuenta()
    {
        super();
    }

    public Cuenta(Integer idUsuario)
    {
        super(idUsuario);
    }

    public Integer getId() {
        return id;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio serv) {
        this.servicio = serv;
    }

    public EstadoCuenta getEstadocuenta() {
        return estadocuenta;
    }

    public void setEstadocuenta(EstadoCuenta estado) {
        this.estadocuenta = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String desc) {
        this.descripcion = desc;
    }

//    public List<UsoCuenta> getUsosCuenta() {
//        return usosCuenta;
//    }
//
//    public void setUsosCuenta(List<UsoCuenta> usosCuenta) {
//        this.usosCuenta = usosCuenta;
//    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
