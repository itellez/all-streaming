package com.putacion.allstreaming.service;

import com.putacion.allstreaming.model.Servicio;
import com.putacion.allstreaming.repository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ServicioService implements IServicioService {
    @Autowired
    ServicioRepository servicioRepository;

    @Override
    public Optional<Servicio> obtenerServicio(Integer id) {
        Optional<Servicio> servicio = servicioRepository.findById(id);
        if (servicio.isEmpty())
            throw new NullPointerException("No se encontró el servicio solicitado.");

        return servicio;
    }

    @Override
    public List<Servicio> listarServicios() throws Exception {
        List<Servicio> servicioList = servicioRepository.findAll();
        if (Objects.isNull(servicioList) || servicioList.size() <= 0) {
            throw new NullPointerException("No se encontraron servicios en el sistema.");
        }

        return servicioList;
    }

    @Override
    public Optional<Servicio> guardarServicio(Servicio servicio) throws Exception {
        if (servicioRepository.existsServicioByNombre(servicio.getNombre()))
            throw new Exception("Ya existe un servicio con ese nombre.");

        Servicio servicioGuardado = servicioRepository.save(servicio);
        return Optional.of(servicioGuardado);
    }

    @Override
    public boolean eliminarServicio(Integer id) {
        Optional<Servicio> servicio = servicioRepository.findById(id);
        if (servicio.isEmpty())
            return false;

        servicioRepository.delete(servicio.get());
        return true;
    }

    @Override
    public boolean quitarServicio(Integer id) {
        Optional<Servicio> servicio = servicioRepository.findById(id);
        if (servicio.isEmpty())
            return false;

        servicio.get().setCambiosAuditoria();
        servicio.get().setVigente(false);

        Servicio servicioEliminado = servicioRepository.save(servicio.get());
        return (!Objects.isNull(servicioEliminado));
    }
}
