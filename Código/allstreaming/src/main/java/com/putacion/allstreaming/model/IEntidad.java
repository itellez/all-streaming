package com.putacion.allstreaming.model;

import java.sql.Timestamp;
import java.util.Date;

public interface IEntidad {
    public Timestamp getFechaCreacion();

    public Timestamp getFechaMod();

    public void setFechaMod(Timestamp fecha);

    public Integer getUsuarioIdCreador();

    public Integer getUsuarioIdMod();

    public void setUsuarioIdMod(Integer idUsuario);

    public boolean getVigente();

    public void setVigente(boolean vigencia);
}
