package com.putacion.allstreaming.controller;

import com.putacion.allstreaming.model.ArriendoCuenta;
import com.putacion.allstreaming.service.IArriendoCuentaService;
import com.putacion.allstreaming.utils.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/arriendos")
public class ArriendoCuentaController {

    @Autowired
    private IArriendoCuentaService arriendoCuentaService;

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity obtenerArriendoCuenta(@PathVariable Integer id) {
        try {
            Optional<ArriendoCuenta> arriendoCuenta = arriendoCuentaService.obtenerArriendoCuenta(id);
            if (arriendoCuenta.isEmpty())
                return ResponseHandler.generateResponse("No fue posible obtener el arriendo solicitado.", HttpStatus.NOT_FOUND);

            return new ResponseEntity(arriendoCuenta.get(), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar obtener el arriendo (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/cuenta/{idCuenta}/usuario/{idUsuario}/dias/{dias}", produces = "application/json")
    public ResponseEntity registrarArriendoDias(@PathVariable Integer idCuenta, @PathVariable Integer idUsuario, @PathVariable Integer dias) {
        try {
            Optional<ArriendoCuenta> arriendoCuenta = arriendoCuentaService.registrarArriendoDias(idCuenta, idUsuario, dias);
            if (arriendoCuenta.isEmpty())
                return ResponseHandler.generateResponse("No se pudo crear el registro de arriendo indicado.", HttpStatus.BAD_REQUEST);

            return new ResponseEntity(arriendoCuenta.get(), HttpStatus.CREATED);
        } catch (NullPointerException nex) {
            return new ResponseEntity(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar registrar el arriendo solicitado (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/cuenta/{idCuenta}/usuario/{idUsuario}/horas/{horas}", produces = "application/json")
    public ResponseEntity registrarArriendoHoras(@PathVariable Integer idCuenta, @PathVariable Integer idUsuario, @PathVariable Integer horas) {
        try {
            Optional<ArriendoCuenta> arriendoCuenta = arriendoCuentaService.registrarArriendoHoras(idCuenta, idUsuario, horas);
            if (arriendoCuenta.isEmpty())
                return ResponseHandler.generateResponse("No se pudo crear el registro de arriendo indicado.", HttpStatus.BAD_REQUEST);

            return new ResponseEntity(arriendoCuenta.get(), HttpStatus.CREATED);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar registrar el arriendo solicitado (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity eliminarArriendoCuenta(@PathVariable Integer id) {
        try {
            if (arriendoCuentaService.eliminarArriendoCuenta(id))
                return ResponseHandler.generateResponse("Se borró exitosamente el arriendo.", HttpStatus.OK);

            return ResponseHandler.generateResponse("No fue posible eliminar el registro de arriendo.", HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar eliminar el arriendo indicado (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
