package com.putacion.allstreaming.controller;

import com.putacion.allstreaming.model.Servicio;
import com.putacion.allstreaming.service.IServicioService;
import com.putacion.allstreaming.utils.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/servicios")
public class ServicioController {
    @Autowired
    private IServicioService servicioService;

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity obtenerServicio(@PathVariable Integer id) {
        try {
            Optional<Servicio> servicio = servicioService.obtenerServicio(id);

            return new ResponseEntity(servicio.get(), HttpStatus.OK);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error cargando los servicios (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/", produces = "application/json")
    public ResponseEntity listarServicios() {
        try {
            List<Servicio> listaServicios = servicioService.listarServicios();

            return new ResponseEntity(listaServicios, HttpStatus.OK);
        } catch (NullPointerException nex) {
            return ResponseHandler.generateResponse(nex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error cargando los servicios (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/", produces = "application/json")
    public ResponseEntity crearServicio(@RequestBody Servicio servicio) {
        try {
            Optional<Servicio> servicioGuardado = servicioService.guardarServicio(servicio);
            if (servicioGuardado.isEmpty())
                return ResponseHandler.generateResponse("No fue posible crear el servicio.", HttpStatus.BAD_REQUEST);

            return new ResponseEntity(servicioGuardado.get(), HttpStatus.CREATED);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error al intentar crear el servicio (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity removerServicio(@PathVariable Integer id) {
        try {
            if (servicioService.eliminarServicio(id)) {
                return ResponseHandler.generateResponse("Servicio eliminado exitosamente.", HttpStatus.OK);
            }

            return ResponseHandler.generateResponse("No se pudo eliminar el servicio solicitado.", HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return ResponseHandler.generateResponse("Ocurrió un error inesperado al intentar eliminar el servicio (" + ex.getMessage() + ").", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
