package com.putacion.allstreaming.service;

import com.putacion.allstreaming.domain.Enumeraciones;
import com.putacion.allstreaming.model.ArriendoCuenta;
import com.putacion.allstreaming.model.Cuenta;
import com.putacion.allstreaming.model.Usuario;
import com.putacion.allstreaming.repository.ArriendoCuentaRepository;
import com.putacion.allstreaming.repository.CuentaRepository;
import com.putacion.allstreaming.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArriendoCuentaService implements IArriendoCuentaService {
    @Autowired
    private ArriendoCuentaRepository arriendoCuentaRepository;
    @Autowired
    private CuentaRepository cuentaRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Optional<ArriendoCuenta> obtenerArriendoCuenta(Integer idArriendocuenta) throws Exception {
        Optional<ArriendoCuenta> arriendoCuenta = arriendoCuentaRepository.findById(idArriendocuenta);

        return arriendoCuenta;
    }

    @Override
    public Optional<ArriendoCuenta> registrarArriendoDias(Integer idCuenta, Integer idUsuario, Integer dias) throws Exception {
        Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
        Optional<Cuenta> cuenta = cuentaRepository.findById(idCuenta);

        if (usuario.isEmpty() || cuenta.isEmpty()) {
            throw new NullPointerException("No se pudo encontrar uno de los componentes del arriendo");
        }

        if (!cuenta.get().getEstadocuenta().getNombre().equalsIgnoreCase(Enumeraciones.Estado_Cuenta.DISPONIBLE.name())) {
            throw new Exception("La cuenta no está disponible (estado: " + cuenta.get().getEstadocuenta().getNombre() + ")");
        }

        ArriendoCuenta arriendoCuenta = new ArriendoCuenta();
        arriendoCuenta.setCuenta(cuenta.get());
        arriendoCuenta.setUsuario(usuario.get());
        arriendoCuenta.setDias(dias);
        arriendoCuenta.setHoras(0);

        ArriendoCuenta arriendoGuardado = arriendoCuentaRepository.save(arriendoCuenta);
        return Optional.of(arriendoGuardado);
    }

    @Override
    public Optional<ArriendoCuenta> registrarArriendoHoras(Integer idCuenta, Integer idUsuario, Integer horas) throws Exception {
        Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
        Optional<Cuenta> cuenta = cuentaRepository.findById(idCuenta);
        if (usuario.isEmpty() || cuenta.isEmpty()) {
            throw new NullPointerException("Algunos de los componentes de la cuenta no pudieron ser identificados");
        }
        if (!cuenta.get().getEstadocuenta().getNombre().equalsIgnoreCase(Enumeraciones.Estado_Cuenta.DISPONIBLE.name())) {
            throw new Exception("La cuenta se encuentra no disponible (estado: " + cuenta.get().getEstadocuenta().getNombre() + ")");
        }

        ArriendoCuenta arriendoCuenta = new ArriendoCuenta();
        arriendoCuenta.setCuenta(cuenta.get());
        arriendoCuenta.setUsuario(usuario.get());
        arriendoCuenta.setHoras(horas);
        arriendoCuenta.setDias(0);

        ArriendoCuenta arriendoGuardado = arriendoCuentaRepository.save(arriendoCuenta);
        return Optional.of(arriendoGuardado);
    }

    @Override
    public boolean eliminarArriendoCuenta(Integer id) {
        Optional<ArriendoCuenta> arriendoCuenta = arriendoCuentaRepository.findById(id);
        if (arriendoCuenta.isEmpty())
            return false;

        arriendoCuentaRepository.delete(arriendoCuenta.get());
        return true;
    }
}
