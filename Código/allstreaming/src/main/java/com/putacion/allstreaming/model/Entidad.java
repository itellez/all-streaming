package com.putacion.allstreaming.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.putacion.allstreaming.configuration.DetallesUsuario;
import com.putacion.allstreaming.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.util.Date;

public abstract class Entidad implements IEntidad {

    @JsonIgnore
    @Column(columnDefinition = "fechacreacion")
    private final Timestamp fechacreacion;
    @JsonIgnore
    @Column(columnDefinition = "fechamod")
    private Timestamp fechamod;
    @JsonIgnore
    @Column(columnDefinition = "usuario_id_creador")
    private final Integer usuario_id_creador;
    @JsonIgnore
    @Column(columnDefinition = "usuario_id_mod")
    private Integer usuario_id_mod;
    @JsonIgnore
    private boolean vigente;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Entidad() {
        this.fechacreacion = new Timestamp(new Date().getTime());
        this.fechamod = new Timestamp(new Date().getTime());
        this.usuario_id_creador = 1;
        this.usuario_id_mod = 1;
        this.vigente = true;
    }

    public Entidad(Integer idUsuario)
    {
        this.fechacreacion = new Timestamp(new Date().getTime());
        this.fechamod = new Timestamp(new Date().getTime());
        this.usuario_id_creador = idUsuario;
        this.usuario_id_mod = idUsuario;
        this.vigente = true;
    }

    @Override
    @JsonIgnore
    public Timestamp getFechaCreacion() {
        return fechacreacion;
    }

    @Override
    @JsonIgnore
    public Timestamp getFechaMod() {
        return fechamod;
    }

    @Override
    public void setFechaMod(Timestamp fecha) {
        this.fechamod = fecha;
    }

    @Override
    @JsonIgnore
    public Integer getUsuarioIdCreador() {
        return usuario_id_creador;
    }

    @Override
    @JsonIgnore
    public Integer getUsuarioIdMod() {
        return usuario_id_mod;
    }

    @Override
    public void setUsuarioIdMod(Integer idUsuario) {
        this.usuario_id_mod = idUsuario;
    }

    @Override
    @JsonIgnore
    public boolean getVigente() {
        return vigente;
    }

    @Override
    public void setVigente(boolean vigencia) {
        this.vigente = vigencia;
    }

    public void setCambiosAuditoria() {
        this.fechamod = new Timestamp(new Date().getTime());

        Object obj = SecurityContextHolder.getContext().getAuthentication();
        if (obj instanceof DetallesUsuario)
        {
            obj = (DetallesUsuario)obj;
            String usuario = ((DetallesUsuario) obj).getUsername();
            Integer id = usuarioRepository.findByEmail(usuario).get().getId();
            this.usuario_id_mod = id;
        }
        else {
            this.usuario_id_mod = 1;
        }
    }
}
