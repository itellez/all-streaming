package com.putacion.allstreaming.model;

import javax.persistence.*;

@Entity
public class Servicio extends Entidad {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String nombre;
    private String url;
    private Float preciomensual;

    public Servicio(){
        super();
    }

    public Servicio(Integer idUsuario) {
        super(idUsuario);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Float getPreciomensual() {
        return preciomensual;
    }

    public void setPreciomensual(Float preciomensual) {
        this.preciomensual = preciomensual;
    }
}
