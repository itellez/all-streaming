package com.putacion.allstreaming.configuration;

import com.putacion.allstreaming.model.Usuario;
import com.putacion.allstreaming.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServicioDetallesUsuario implements UserDetailsService {

    @Autowired
    private UsuarioRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Usuario existingUser = userRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("Usuario no encontrado"));

        return new org.springframework.security.core.userdetails.User(
                existingUser.getEmail(), existingUser.getClave(), new ArrayList<>());
    }

}
